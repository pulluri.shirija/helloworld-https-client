/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "helloworld-https-client.h"

#include <libsoup/soup.h>
#include <mildenhall/mildenhall.h>

#define IFCONFIG_CO_JSON_URI "https://ifconfig.co/json"

struct _HlwHttpsClient
{
  /*< private >*/
  GApplication parent;

  ClutterActor *stage;  /* owned */
  SoupSession *session; /* owned */

  MildenhallButtonSpeller *button; /* unowned */

  gchar *ip_address; /* owned */
  gchar *country;    /* owned */
  gchar *city;       /* owned */
};

G_DEFINE_TYPE (HlwHttpsClient, hlw_https_client, G_TYPE_APPLICATION);

enum
{
  COLUMN_TEXT = 0,
  COLUMN_LAST = COLUMN_TEXT,
  N_COLUMN,
};

static gboolean
on_stage_destroy (ClutterActor *stage,
                  gpointer user_data)
{
  GApplication *app = user_data;

  g_debug ("The main stage is destroyed.");

  /* chain up */
  CLUTTER_ACTOR_GET_CLASS (stage)
      ->destroy (stage);

  /* The main window is detroyed (closed) so the application
   * needs to be released */
  g_application_release (app);

  return TRUE;
}

static void
set_values (JsonObject *json_object,
            const gchar *member_name,
            JsonNode *member_node,
            gpointer user_data)
{
  HlwHttpsClient *self = user_data;

  if (g_strcmp0 (member_name, "ip") == 0)
    {
      g_free (self->ip_address);
      self->ip_address = json_node_dup_string (member_node);
    }
  else if (g_strcmp0 (member_name, "country") == 0)
    {
      g_free (self->country);
      self->country = json_node_dup_string (member_node);
    }
  else if (g_strcmp0 (member_name, "city") == 0)
    {
      g_free (self->city);
      self->city = json_node_dup_string (member_node);
    }
}

static void
message_cb (GObject *source_object,
            GAsyncResult *result,
            gpointer user_data)
{
  SoupSession *session = SOUP_SESSION (source_object);
  g_autoptr (GInputStream) stream = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (JsonParser) parser = json_parser_new ();
  g_autofree gchar *display_message = NULL;
  JsonNode *root_node;
  JsonObject *main_obj;

  HlwHttpsClient *self = user_data;

  stream = soup_session_send_finish (session, result, &error);

  if (error)
    {
      g_warning ("%s: Failed to send a message (reason: %s)",
                 G_STRFUNC,
                 error->message);
      return;
    }

  if (!json_parser_load_from_stream (parser, stream, NULL, &error))
    {
      g_warning ("%s: Failed to parse JSON from stream (reason: %s)",
                 G_STRFUNC,
                 error->message);
      return;
    }

  root_node = json_parser_get_root (parser);

  if (!JSON_NODE_HOLDS_OBJECT (root_node))
    {
      g_warning ("The root node doesn't hold any JSON object.");
      return;
    }

  main_obj = json_node_get_object (root_node);

  json_object_foreach_member (main_obj, set_values, self);

  display_message = g_strdup_printf ("My IP is %s from %s, %s)",
                                     self->ip_address,
                                     self->city,
                                     self->country);

  v_button_speller_set_text (CLUTTER_ACTOR (self->button), display_message);
}

static void
button_pressed_cb (MildenhallButtonSpeller *button,
                   gpointer user_data)
{
  g_autoptr (SoupMessage) msg = soup_message_new ("GET", IFCONFIG_CO_JSON_URI);

  HlwHttpsClient *self = user_data;

  soup_session_send_async (self->session, msg, NULL, message_cb, self);
}

static void
startup (GApplication *app)
{
  g_autoptr (GObject) widget_object = NULL;
  g_autoptr (ThornburyItemFactory) item_factory = NULL;
  g_autofree gchar *widget_properties_file = NULL;
  ThornburyModel *model;

  HlwHttpsClient *self = HLW_HTTPS_CLIENT (app);

  g_application_hold (app);

  /* chain up */
  G_APPLICATION_CLASS (hlw_https_client_parent_class)
      ->startup (app);

  /* build ui components */
  self->stage = mildenhall_stage_new (g_application_get_application_id (app));

  g_signal_connect (G_OBJECT (self->stage),
                    "destroy", G_CALLBACK (on_stage_destroy),
                    self);

  widget_properties_file = g_build_filename (PKGDATADIR,
                                             "button_speller_text_prop.json",
                                             NULL);

  item_factory = thornbury_item_factory_generate_widget_with_props (
      MILDENHALL_TYPE_BUTTON_SPELLER,
      widget_properties_file);

  g_object_get (item_factory,
                "object", &widget_object,
                NULL);

  self->button = MILDENHALL_BUTTON_SPELLER (widget_object);

  model = THORNBURY_MODEL (
      thornbury_list_model_new (N_COLUMN,
                                G_TYPE_STRING,
                                NULL,
                                -1));

  thornbury_model_append (model,
                          COLUMN_TEXT, "Click to retrieve information!",
                          -1);

  g_object_set (self->button,
                "model", model,
                NULL);

  g_clear_object (&model);

  clutter_actor_add_child (self->stage, CLUTTER_ACTOR (self->button));

  g_signal_connect (self->button,
                    "button-press", G_CALLBACK (button_pressed_cb),
                    self);
}

static void
activate (GApplication *app)
{
  HlwHttpsClient *self = HLW_HTTPS_CLIENT (app);

  clutter_actor_show (self->stage);
}

static void
hlw_https_client_dispose (GObject *object)
{
  HlwHttpsClient *self = HLW_HTTPS_CLIENT (object);

  g_clear_object (&self->session);
  g_clear_object (&self->stage);

  g_clear_pointer (&self->ip_address, g_free);
  g_clear_pointer (&self->city, g_free);
  g_clear_pointer (&self->country, g_free);

  G_OBJECT_CLASS (hlw_https_client_parent_class)
      ->dispose (object);
}

static void
hlw_https_client_class_init (HlwHttpsClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->dispose = hlw_https_client_dispose;

  app_class->startup = startup;
  app_class->activate = activate;
}

static void
hlw_https_client_init (HlwHttpsClient *self)
{
  self->session = soup_session_new ();
}
